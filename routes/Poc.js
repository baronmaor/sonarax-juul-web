const Riffwaves = require('../services/Riffwaves');
const fs = require('fs');

module.exports = (sonaraxSdk, io) => {
  let SN = `cRIlf8F9kcHnbxaC4HzSHtnlCmdy1bVFaHRk341YAuRO4ASecuhWVTqGE5OOCoOz`;
  let DU = `yiV1n8uQWkdDglYFLNDl32ELNYueT3s7KNbY9EVy17CLE6eyHq5cAStgkLutd4WP`;

  const convertToWavBuffer = async encoded => {
    const encodedWave = new Riffwaves(encoded, 48000); //should be always 48000 (safari issues)
    const encodedBuffer = Buffer.from(encodedWave.wav);
    return encodedBuffer;
  };

  const decode = async (aSampleAudio, protocol) => {
    let json = new Int32Array(4096);
    let c = 0;

    for (let key in aSampleAudio) {
      json[c] = aSampleAudio[key] * 10; // multiple by 10 (saving data on socket)
      c++;
    }

    try {
      if (protocol == 'd') {
        const res = await sonaraxSdk.decode(json);
        return res;
      } else {
        let res = await sonaraxSdk.symbolDetect(json);
        return res;
      }
    } catch (error) {
      console.log(error);
    }
  };


  io.on('connection', socket => {
    console.log('Juul socket connected');



    socket.on('symbolEncode', async e => {
      console.log(`Symbol encode: ${__filename}`);
      try {
        const encoded = await sonaraxSdk.encodeSymbol(10);
        const encodedBuffer = await convertToWavBuffer(encoded);
        socket.emit('encodedSymbol', encodedBuffer);
        socket.emit('status', {
          result: '',
          err: false,
          msg: `-- Symbol hello encoded OK --`
        });
      } catch (error) {
        socket.emit('status', {
          result: '',
          err: true,
          msg: `-- ${error} --`
        });
      }
    });
    socket.on('decodeSerialData', async e => {
      const res = await decode(e.data, e.type);
      console.log(res);
      if (res === SN) {
        socket.emit('status', {
          result: res,
          err: false,
          msg: `-- Server serial pass --`
        });
      } else if (res !== SN && res !== undefined) {
        socket.emit('status', {
          result: '',
          err: true,
          msg: `-- serial not correct --`
        });
      } else {
        // socket.emit('status', {
        //   result: '',
        //   err: false,
        //   msg: ``
        // });
      }
    });

    socket.on('encodeDUData', async e => {




      // reseting the env for ultraSonic after dongle mic issue will be solved 


      // await sonaraxSdk.resetDataStartBin1k(
      //   sonaraxSdk.SampleRate.Hz_48000,
      //   sonaraxSdk.DataFrequencyRange.VERY_HIGH
      // );

      await sonaraxSdk.setVolume(0.38); //setting sdk volume 

      try {
        const encoded = await sonaraxSdk.encodeData(DU);
        const encodedBuffer = await convertToWavBuffer(encoded);
        let time = new Date().getTime();
        let wavFile = fs.createWriteStream(
          `${__dirname}/../dist/juul/assets/wav/${time}.wav`
        );
        const creatingFile = wavFile.write(encodedBuffer);
        socket.emit('encodedDUData', time);
        socket.emit('status', {
          result: '',
          err: false,
          msg: `-- Data DU encoded OK --`
        });
      } catch (error) {
        socket.emit('status', {
          result: '',
          err: true,
          msg: `-- ${error} --`
        });
      }
    });
    //setting symbol as sonic 
    socket.on('setSymbolSonic', async e => {
      sonaraxSdk.setSymbolStartBin1k(
        sonaraxSdk.SampleRate.Hz_48000,
        sonaraxSdk.SymbolFrequencyRange.VERY_HIGH,
        6000
      );
    });

    //setting data as sonic 
    socket.on('setDataSonic', async e => {
      sonaraxSdk.setDataStartBin1k(
        sonaraxSdk.SampleRate.Hz_48000,
        sonaraxSdk.DataFrequencyRange.VERY_HIGH,
        6000
      );

    });
    socket.on('resetSymbol', async e => {

      // dongle mic issues 

      // sonaraxSdk.resetSymbolStartBin1k(
      //   sonaraxSdk.SampleRate.Hz_48000,
      //   sonaraxSdk.SymbolFrequencyRange.VERY_HIGH
      // );
    });

    socket.on('decodeSymbolStatus', async e => {

      const res = await decode(e.data, e.type, socket);
      console.log(res)
      if (res == 12) {
        socket.emit('decodedSymbolStatus', {
          result: res,
          err: false,
          msg: `-- Operation complete successfully --`
        });
      } else if (res == 14) {
        socket.emit('decodedSymbolStatus', {
          result: res,
          err: true,
          msg: `-- Dongle return error --`
        });
      }
    });
  });
};
