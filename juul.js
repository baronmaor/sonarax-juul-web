const sonaraxSdk = require('../sonarax-juul-sdk/Package/index');
const express = require('express');
const config = require('./config');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');
const server = require('http').createServer();
const fs = require('fs');

const io = require('socket.io')(server, {
  serveClient: false,
  pingInterval: 25000,
  pingTimeout: 5000,
  allowUpgrades: true
});

server.listen(1171);

app.use(cors());
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'dist/juul')));

app.get('/sound', function (req, res) {
  console.log(`Sending sound: ${__filename}`);
  const soundToPlay = req.query.id;
  const file = `${__dirname}/dist/juul/assets/wav/${soundToPlay}.wav`;

  fs.exists(file, exists => {
    if (exists) {
      const rstream = fs.createReadStream(file);
      res.writeHead(206, {
        'Content-Type': 'audio/wav'
      });
      rstream.pipe(res);
      setTimeout(() => {
        fs.unlinkSync(file);

      }, 2500);
    } else {
      console.log('errorrorr');
      res.send('Error - 404');
      res.end();
    }
  });
});

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, '/dist/juul/index.html'));
});

// Starting the server
app.listen(config.PORT, () => {
  const init = sonaraxSdk.init('MCtuMn+PRg6wpmx3WQ2s3288', 'com.sonarax.juul.web');

  if (init) {
    require('./routes/Poc')(sonaraxSdk, io);

    //setting global sdk configuration
    sonaraxSdk.setChannel(sonaraxSdk.Channel.CHANNEL_NONE);
    sonaraxSdk.setSampleRate(sonaraxSdk.SampleRate.Hz_48000);
    sonaraxSdk.setECCLevel(sonaraxSdk.EccLevel.MID);


    // Setting data configurations
    sonaraxSdk.setDataMode(sonaraxSdk.DataMode.C);
    sonaraxSdk.setECCLevel(sonaraxSdk.EccLevel.OFF);
    sonaraxSdk.setDataStartBin1k(
      sonaraxSdk.SampleRate.Hz_48000,
      sonaraxSdk.DataFrequencyRange.VERY_HIGH,
      6000
    );



    // Setting symbol configurations
    sonaraxSdk.setSymbolMode(sonaraxSdk.SymbolMode.D);
    sonaraxSdk.resetSymbolStartBin1k(
      sonaraxSdk.SampleRate.Hz_48000,
      sonaraxSdk.DataFrequencyRange.VERY_HIGH
    );

    console.log(`Juul started on port:  ${config.PORT}`);
  } else {
    console.log(`Sdk init failed`);
    process.exit();
  }
});
