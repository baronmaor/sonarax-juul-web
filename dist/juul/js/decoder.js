var mediastream;
var mediaRecorder;
var audioConfig;
var sample_Rate = 48000;

const audioTypes = [
  {
    mimeType: 'audio/wav'
  },
  {
    mimeType: 'audio/webm'
  }
];

const setFreq = async function () {
  var Freq = document.getElementById('startFreq').value;
  startFreq = Freq;
  startFfreqTxt.innerText = `${Freq}`;
  var f = { data: Freq, type: 'setFreq' };
  socket.emit('setFreq', f);
};

const startRecording = function (type) {

  navigator.mediaDevices
    .getUserMedia({
      audio: {
        autoGainControl: !1,
        echoCancellation: !1,
        noiseSuppression: !1,
        sampleRate: sample_Rate,
      },
      video: false
    })
    .then(handleSuccess)
    .then(function () {
      for (var index = 0; index < audioTypes.length; index++) {
        var options = {
          mimeType: audioTypes[index],
        };
        try {
          mediaRecorder = new window.MediaRecorder(mediastream, options);
          audioConfig = audioTypes[index];
          break;
        } catch (err) { }
      }
      mediaRecorder.start(sample_Rate, type);
    })
    .catch(handleError);
};

const stopRecording = function () {

  mediaRecorder.stop();
  mediastream.getTracks().forEach(function (track) {
    track.stop();
  });

};

const handleSuccess = function (stream) {
  mediastream = stream;
};

const handleError = function (err) {
  console.log('error: ' + err);
};
