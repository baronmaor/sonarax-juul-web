var steps = ['4', '5', '6', '7'];
var ErrTxt = {
  4: "Something went wrong, please try again in a few seconds.",
  5: "Timeout error, please try again",
  5_1: "The dongle serial key is invalid!",
  6: "Something went wrong, please try again in a few seconds.",
  7: "Error code received from dongle."
}
var stepperFlow = function (step) {
  var position = document.getElementById(step);

  steps.forEach(function (s) {
    document.getElementById(s).style.fontWeight = '400';
  });
  position.style.fontWeight = '600';
};

var stepperError = function (step, err = '') {
  var position = document.getElementById(step).children;
  var roundStepper = position[0].getElementsByClassName('roundStepper')[0];
  var innerStepper = position[0].getElementsByClassName('innerStepper')[0];
  var errTxt = position[1].getElementsByClassName('errTxt')[0];
  roundStepper.style.border = '2px solid #ff4444';
  innerStepper.style.color = '#ff4444';
  console.log(step + err);

  errTxt.innerText = ErrTxt[step + err]
};

var stepperSuccess = function (step) {
  var position = document.getElementById(step).children;
  var roundStepper = position[0].getElementsByClassName('roundStepper')[0];
  var innerStepper = position[0].getElementsByClassName('innerStepper')[0];
  roundStepper.style.border = '2px solid #00C851';
  innerStepper.style.color = '#00C851';
};
