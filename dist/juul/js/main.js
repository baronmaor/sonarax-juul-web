

// variable to indicate if running on an Apple device
var iOS = /iPad|Macintosh|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

//================================
//  Variables
//================================

var HandShakeSound;
var startBtnNeon;
var timeForHelloSymbol = iOS ? 500 : 2000; // Milli
var timeForSNReceive = iOS ? 8500 : 8500;
var timeForDUData = iOS ? 8500 : 8500;
var timeForconfirmationSymbol = 10000;
var isInProcess = false;
var isPlaying = false;
var isRecording = false;
var isErrorStat = false;



startBtnNeon = document.getElementById('startBtnNeon');
startRestartBtn = document.getElementById('startBtn');



//================================
//  Functions
//================================

/***
 * called when device connects to server on socket.
 * moves the SDK to function in the audible range.
 */
socket.on('connect', function () {
  socket.emit('setDataSonic');
});


/***
 * switches the flow on and off when called
 */
var operationFlow = function () {
  switch (isInProcess) {
    case false:
      startFlow();
      break;

    default:
      stopFlow();
      break;
  }
};

/***
 * Start the activation flow when called
 */
var startFlow = function () {
  startRestartBtn.onclick = function () {
    location.reload();
  };
  startRestartBtn.innerHTML =
    'Restart<span class="btnNeon" id="startBtnNeon"></span>';
  startBtnNeon = document.getElementById('startBtnNeon');

  // transmit handshake symbol to intiate flow

  isPlaying = true;
  HandShakeSound.play(); //playing symbol 10 
  stepperFlow(4);
  isInProcess = true;

  // set timer for timeout for the handshake phase

  setTimeout(function () {
    console.log("stopped playing 'hello' ");
    isPlaying = false;
  }, timeForHelloSymbol);

  startBtnNeon.style.background = '#42DDFF 0% 0% no-repeat padding-box';
  console.log(
    `starting to play 'hello symbol' for ${timeForHelloSymbol / 1000} sec`
  );
};

/***
 * stops the flow when called
 */
var stopFlow = function () {
  isInProcess = false;
  startBtnNeon.style.background = 'black 0% 0% no-repeat padding-box';
};


/***
 * start listening to serial number recieving from dongle key -> if ok start sending Dongle unlock code  
 * */
var startDecoding = function () {
  console.log(`starting to record for: ${timeForSNReceive / 1000} sec `);
  stepperFlow(5);
  isRecording = true;
  isErrorStat = true;
  startRecording('d'); // recording serial number of the device 

  // setting a timeout for receiving serial key phase
  setTimeout(function () {
    console.log(`stop recording`);
    stopRecording();
    isRecording = false;

    // in case detection of serial was successful
    if (!isErrorStat) {
      stepperSuccess(5);
      stepperFlow(6);
      socket.emit('encodeDUData');

      // in case detection of serial failed
    } else {
      stepperError(5);
      console.log(`Stopping flow, data not ok`);
    }
  }, timeForSNReceive);
};

/***
 * start listening for final activation result 
 */
var startRecordingStatus = function () {
  console.log(
    `starting to record for: ${timeForconfirmationSymbol / 1000} sec `
  );

  isRecording = true;
  isErrorStat = true;

  startRecording('s'); //recording symbol status - 12:ok  14:error 

  // set end action in the end of detection
  setTimeout(function () {
    console.log(`stop recording`);
    stopRecording();
    //  socket.emit('resetSymbol'); // not in use (dongle mic issues)
    isRecording = false;

    // in case activation failed
    if (isErrorStat) {
      stepperError(7);
    }
  }, timeForconfirmationSymbol);
};

// get activation status from Sonarax server
socket.on('status', function (e) {
  isErrorStat = e.err;
  console.log(e.msg);
});


/***
 * start sending dongle unlock code 
 */
socket.on('encodedDUData', function (e) {
  console.log(
    `Start playing dongle unlock code for ${timeForDUData / 1000} sec`
  );

  new Howl({
    src: `/sound/?id=${e}`,
    format: ['wav'],
    onend: function () {
      this.unload();
    },
    volume: 0.5,
  }).play();

  // start timer for phase advance after sending unlcok code
  setTimeout(function () {
    socket.emit('setSymbolSonic');
    stepperSuccess(6);
    stepperFlow(7);
    startRecordingStatus();
  }, timeForDUData);
});

/***
 * handle activation result when received from Sonarax server
 */
socket.on('decodedSymbolStatus', async function (e) {
  // case: successful activation
  if (e.result == 12) {
    isErrorStat = false;
    stepperSuccess(7);
    // case: failed activation
  } else {
    stepperError(7);
  }
});

/***
 * test device speakers function
 */
var testSpeakers = function () {
  //defining handshake symbol and saving it for later
  HandShakeSound = new Howl({
    src: ['./assets/wav/10_sonic.wav'],
    format: ['wav'],
    onend: function () {
      if (isPlaying) {
        this.play();
      } else {
        this.stop();
        this.unload();
        stepperSuccess(4);
        startDecoding();
      }
    },
  });

  var position = document.getElementById('speakerDevice').children;
  var roundStepper = position[0].getElementsByClassName('roundStepper')[0];
  var innerStepper = position[0].getElementsByClassName('innerStepper')[0];
  var spanInstructions = position[1].getElementsByClassName('stepperInstructions')[0];

  var playTestSound = new Howl({
    src: `./assets/wav/bell.wav`,
    loop: false,
    format: ['wav'],
    volume: 0.30,

    //handle speaker test dialog result
    onend: function () {

      // case speaker test successful, set UI
      if (confirm('"Did You Hear That Sound?')) {
        roundStepper.style.border = '2px solid #00C851';
        innerStepper.style.color = '#00C851';
        spanInstructions.style.color = '#00C851';
        startBtnNeon.style.display = 'block';
        startRestartBtn.disabled = false;

        // case speaker test failed, disable flow start UI
      } else {
        startBtnNeon.style.display = 'none';
        startRestartBtn.disabled = true;
        roundStepper.style.border = '2px solid #ff4444';
        innerStepper.style.color = '#ff4444';
        spanInstructions.style.color = '#ff4444';

      }
    }
  })
  playTestSound.play();
}

