const RIFFWAVE = function (data, sampleRate) {
    this.data = []; // Array containing audio samples

    // converting to 16bit since native is 5.0.0 -- decode
    let tmp = [];
    for (let i = 0; i < data.length; i++) {
        let sample = data[i];
        tmp.push((sample / (2 ** 31 + 1)) * 32767);
    }
    data = tmp;

    this.wav = []; // Array containing the generated wave file
    this.dataURI = ''; // http://en.wikipedia.org/wiki/Data_URI_scheme
    this.sampleRate = sampleRate;

    this.header = {
        // OFFS SIZE NOTES
        chunkId: [0x52, 0x49, 0x46, 0x46], // 0    4    "RIFF" = 0x52494646
        chunkSize: 0, // 4    4    36+SubChunk2Size = 4+(8+SubChunk1Size)+(8+SubChunk2Size)
        format: [0x57, 0x41, 0x56, 0x45], // 8    4    "WAVE" = 0x57415645
        subChunk1Id: [0x66, 0x6d, 0x74, 0x20], // 12   4    "fmt " = 0x666d7420
        subChunk1Size: 16, // 16   4    16 for PCM
        audioFormat: 1, // 20   2    PCM = 1
        numChannels: 1, // 22   2    Mono = 1, Stereo = 2...
        sampleRate: this.sampleRate, // 24   4    8000, 44100...
        byteRate: 0, // 28   4    SampleRate*NumChannels*BitsPerSample/8
        blockAlign: 0, // 32   2    NumChannels*BitsPerSample/8
        bitsPerSample: 16, // 34   2    8 bits = 8, 16 bits = 16
        subChunk2Id: [0x64, 0x61, 0x74, 0x61], // 36   4    "data" = 0x64617461
        subChunk2Size: 0 // 40   4    data size = NumSamples*NumChannels*BitsPerSample/8
    };

    function u32ToArray(i) {
        return [i & 0xff, (i >> 8) & 0xff, (i >> 16) & 0xff, (i >> 24) & 0xff];
    }

    function u16ToArray(i) {
        return [i & 0xff, (i >> 8) & 0xff];
    }

    function split16bitArray(data) {
        let r = [];
        let j = 0;
        let len = data.length;
        for (let i = 0; i < len; i++) {
            r[j++] = data[i] & 0xff;
            r[j++] = (data[i] >> 8) & 0xff;
        }

        return r;
    }

    this.Make = function (data) {
        if (data instanceof Array) this.data = data;
        this.header.blockAlign =
            (this.header.numChannels * this.header.bitsPerSample) >> 3;
        this.header.byteRate = this.header.blockAlign * this.sampleRate;
        this.header.subChunk2Size =
            this.data.length * (this.header.bitsPerSample >> 3);
        this.header.chunkSize = 36 + this.header.subChunk2Size;

        this.wav = this.header.chunkId.concat(
            u32ToArray(this.header.chunkSize),
            this.header.format,
            this.header.subChunk1Id,
            u32ToArray(this.header.subChunk1Size),
            u16ToArray(this.header.audioFormat),
            u16ToArray(this.header.numChannels),
            u32ToArray(this.header.sampleRate),
            u32ToArray(this.header.byteRate),
            u16ToArray(this.header.blockAlign),
            u16ToArray(this.header.bitsPerSample),
            this.header.subChunk2Id,
            u32ToArray(this.header.subChunk2Size),
            this.header.bitsPerSample == 16 ? split16bitArray(this.data) : this.data
        );
        //this.dataURI = 'data:audio/wav;base64,'+FastBase64.Encode(this.wav);
    };

    if (data instanceof Array) this.Make(data);
}; // end RIFFWAVE

module.exports = RIFFWAVE;
