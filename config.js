module.exports = {
  ENV: process.env.NODE_ENV || 'development',
  PORT: process.env.PORT || 1071,
  URL: process.env.BASE_URL || 'https://localhost:1071'
};

// Handshake symbol sent from web app to dongle: 10
// Result symbol sent from dongle to web app:
// Success - 12
// Failure - 14
// Dongle serial number (sent from dongle to web app) : "cRIlf8F9kcHnbxaC4HzSHtnlCmdy1bVFaHRk341YAuRO4ASecuhWVTqGE5OOCoOz"
// Dongle unlock code (sent from web app to dongle) : "yiV1n8uQWkdDglYFLNDl32ELNYueT3s7KNbY9EVy17CLE6eyHq5cAStgkLutd4WP"
// 1:43

// Configurations:
// Sample Rate : 48Khz
// Frequency Range Audible : 6Khz
// Data Mode: B
// Symbol Mode: D
